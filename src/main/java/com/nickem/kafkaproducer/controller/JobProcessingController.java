package com.nickem.kafkaproducer.controller;

import com.nickem.kafkaproducer.entity.Sale;
import com.nickem.kafkaproducer.service.PlayStoreAppProducer;
import com.nickem.kafkaproducer.service.SaleMessageProducer;

import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/start")
public class JobProcessingController {

    private final JobLauncher jobLauncher;
    private final JobBuilderFactory jobs;
    private final StepBuilderFactory steps;
    private final ItemReader salesItemReader;
    private final ItemReader googlePlayAppItemReader;
    private final SaleMessageProducer saleMessageProducer;
    private final PlayStoreAppProducer playStoreAppProducer;

    @Autowired
    public JobProcessingController(final JobLauncher jobLauncher, final JobBuilderFactory jobs,
                                   final StepBuilderFactory steps, final ItemReader salesItemReader,
                                   final ItemReader googlePlayAppItemReader, final SaleMessageProducer saleMessageProducer,
                                   final PlayStoreAppProducer playStoreAppProducer) {
        this.jobLauncher = jobLauncher;
        this.jobs = jobs;
        this.steps = steps;
        this.salesItemReader = salesItemReader;
        this.googlePlayAppItemReader = googlePlayAppItemReader;
        this.saleMessageProducer = saleMessageProducer;
        this.playStoreAppProducer = playStoreAppProducer;
    }

    @PostMapping("/sales")
    public ResponseEntity createSalesMigration() throws JobParametersInvalidException, JobExecutionAlreadyRunningException,
            JobRestartException, JobInstanceAlreadyCompleteException {

        jobLauncher.run(jobs.get("sales-processing")
                            .incrementer(new RunIdIncrementer())
                            .start(steps.get("sales-step1").<Sale, Sale>chunk(1).reader(salesItemReader)
                                                                                .writer(items -> saleMessageProducer.sendSaleMessageToKafka(
                                                                                        items))
                                                                                .build())
                            .build(),
                        new JobParametersBuilder().addString("JobId", String.valueOf(System.currentTimeMillis()))
                                                  .toJobParameters());

        return ResponseEntity.ok("MigrationHasEnded");
    }

    @PostMapping("/play-store-app")
    public ResponseEntity createPlayStoreAppMigration() throws JobParametersInvalidException, JobExecutionAlreadyRunningException,
            JobRestartException, JobInstanceAlreadyCompleteException {

        jobLauncher.run(jobs.get("play-store-processing")
                            .incrementer(new RunIdIncrementer())
                            .start(steps.get("play-store-step1").<Sale, Sale>chunk(1)
                                           .reader(googlePlayAppItemReader)
                                                                          .writer(items -> playStoreAppProducer.sendMessage(items))
                                                                          .build())
                            .build(),

                        new JobParametersBuilder().addString("JobId", String.valueOf(System.currentTimeMillis()))
                                                  .toJobParameters());

        return ResponseEntity.ok("MigrationHasEnded");
    }

}
