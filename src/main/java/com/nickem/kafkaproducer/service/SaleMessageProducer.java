package com.nickem.kafkaproducer.service;

import com.nickem.kafkaproducer.entity.Sale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class SaleMessageProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SaleMessageProducer.class);

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final String topic;
    private final Integer partition;

    public SaleMessageProducer(final KafkaTemplate<String, String> kafkaTemplate,
                               @Value("${kafka-topics.sale.name}") final String topic,
                               @Value("${kafka-topics.sale.partition}") final Integer partition) {
        this.kafkaTemplate = kafkaTemplate;
        this.topic = topic;
        this.partition = partition;
    }

    public void sendSaleMessageToKafka(List<Sale> sales) {
        LOGGER.info("Sending message: {}", sales);
        kafkaTemplate.send(topic, partition, UUID.randomUUID().toString(), sales.toString());
    }

}
