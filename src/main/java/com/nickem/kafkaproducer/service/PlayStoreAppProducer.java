package com.nickem.kafkaproducer.service;

import com.nickem.entity.PlayStoreApp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Random;
import java.util.UUID;

@Service
public class PlayStoreAppProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayStoreAppProducer.class);

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final String topic;
    private final Integer partitionsSize;

    public PlayStoreAppProducer(final KafkaTemplate<String, String> kafkaTemplate,
                                @Value("${kafka-topics.play-store-app.name}") final String topic,
                                @Value("${kafka-topics.play-store-app.partitions-size}") final Integer partitionsSize) {
        this.kafkaTemplate = kafkaTemplate;
        this.topic = topic;
        this.partitionsSize = partitionsSize;
    }

    public void sendMessage(final Iterable<PlayStoreApp> applications) {
        LOGGER.info("Sending message: {}", applications);

        final Random random = new Random();
        for (final PlayStoreApp app : applications) {
            final Message<PlayStoreApp> message = MessageBuilder
                    .withPayload(app)
                    .setHeader(KafkaHeaders.TOPIC, topic)
                    .setHeader(KafkaHeaders.PARTITION_ID, random.nextInt(partitionsSize))
                    .setHeader("kafka_producedTimestamp", Instant.now().getEpochSecond())
                    .setHeader(KafkaHeaders.MESSAGE_KEY, UUID.randomUUID().toString())
                    .build();

            kafkaTemplate.send(message);
        }
    }
}
