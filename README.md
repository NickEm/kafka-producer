Kafka Postgres Consumer

Installation

1. To build new docker image execute:
 
		./gradlew clean build && docker build -t kafka-producer .
2. To run it execute:
	
		docker run --rm --name kafka-producer -p 8081:8081 --network kafka-local-network -e SERVER_PORT=8081 kafka-producer